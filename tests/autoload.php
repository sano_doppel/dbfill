<?php

spl_autoload_register(function ($class) {
    $filename = __DIR__.'/../' . str_replace('\\', '/', str_replace('DBFill', 'src', $class)) . '.php';
    if (is_file($filename)) {
        include_once($filename);
    }
});