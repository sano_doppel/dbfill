<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Messenger;

use DBFill\Messenger\TranslationFinder\NestedStrategy;
use DBFill\Messenger\TranslationFinder\SimpleStrategy;
use PHPUnit\Framework\TestCase;


final class MessengerTest extends TestCase
{
    /**
     * @covers Messenger, MessageRepository
     */
    public function testMessenger()
    {
        Messenger::setTranslationParameters(
            [
                'translationsExtension' => 'json',
                'translationsPrefix' => 'messages_test',
                'TranslationsDir' => __DIR__,
            ]
        );

        $this->assertEquals(Messenger::translate('nested.test', false, new NestedStrategy()), 'value');
        $this->assertEquals(Messenger::translate('test', false, new NestedStrategy()), 'value');

        $this->assertEquals(Messenger::translate('nested.test', false, new SimpleStrategy()), 'nested.test');
        $this->assertEquals(Messenger::translate('test', false, new SimpleStrategy()), 'value');
    }
}
