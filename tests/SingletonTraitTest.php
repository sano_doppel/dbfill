<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace DBFill\Tests;

use PHPUnit\Framework\TestCase;

include 'Singleton.php';

final class SingletonTraitTest extends TestCase
{
    /**
     * @covers \DBFill\SingletonTrait
     */
    public function testSingletonTrait()
    {
        $instance = Singleton::getInstance();
        $this->assertEquals($instance->getCount(), 0);
        $this->assertEquals($instance->incrementCount(), 1);

        $instance = Singleton::getInstance();
        $this->assertEquals($instance->getCount(), 1);
    }
}
