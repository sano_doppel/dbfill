<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace DBFill\Tests;

use PHPUnit\Framework\TestCase;
use DBFill\Config\Parser\Parser;

/**
 * Class ParserTest
 */
final class ParserTest extends TestCase
{
    const JSON_FILE_PATH = __DIR__.'/config/file.json';

    const XML_FILE_PATH = __DIR__.'/config/file.xml';

    /**
     * @covers \DBFill\Config\Parser\Parser::initial()
     */
    public function testInitialExceptionWrongFile()
    {
        $this->expectException(\Exception::class);
        Parser::initial('config/no_file.xml');
    }

    /**
     * @covers \DBFill\Config\Parser\Parser::initial()
     */
    public function testInitialExceptionWrongFormat()
    {
        $this->expectException(\Exception::class);
        Parser::initial(__DIR__.'/config/file.wrong_format');
    }

    /**
     * @covers \DBFill\Config\Parser\Parser::initial()
     */
    public function testInitial()
    {
        $this->assertInstanceOf(\DBFill\Config\Parser\XMLParser::class, Parser::initial(self::XML_FILE_PATH));
        $this->assertInstanceOf(\DBFill\Config\Parser\JSONParser::class, Parser::initial(self::JSON_FILE_PATH));
    }

    /**
     * @covers \DBFill\Config\Parser\ParserInterface::parse()
     * @depends testInitial
     */
    public function testParse()
    {
        $parsed = Parser::initial(self::XML_FILE_PATH)->parse();
        $this->assertInstanceOf(\SimpleXMLElement::class, $parsed);
        $this->assertTrue(property_exists($parsed, 'test'));
        $this->assertEquals($parsed->test, 'value');

        $parsed = Parser::initial(self::JSON_FILE_PATH)->parse();
        $this->assertInstanceOf(\stdClass::class, $parsed);
        $this->assertTrue(property_exists($parsed, 'test'));
        $this->assertEquals($parsed->test, 'value');
    }
}
