<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace DBFill\Tests;

use DBFill\Config\ConfigInterface;
use DBFill\Config\ConfigProvider;
use PHPUnit\Framework\TestCase;

/**
 * Class ConfigProviderTest
 */
final class ConfigProviderTest extends TestCase
{
    const XML_FILE_PATH = __DIR__.'/config/file.xml';

    /**
     * @covers \DBFill\Config\ConfigProvider::createConfig()
     */
    public function testCreateConfig()
    {
        $provider = new ConfigProvider();

        $this->assertInstanceOf(
            ConfigInterface::class,
            $provider->createConfig(self::XML_FILE_PATH)
        );

        return $provider;
    }

    /**
     * @covers \DBFill\Config\ConfigProvider::getConfig()
     *
     * @depends testCreateConfig
     *
     * @param ConfigProvider $provider
     */
    public function testGetConfig(ConfigProvider $provider)
    {

        $this->assertInstanceOf(
            ConfigInterface::class,
            $provider->getConfig()
        );
    }

    public function testGetMissingParameter()
    {
        $provider = new ConfigProvider();
        $this->assertEquals(
            $provider->getMissingParameter(
                [
                    'parameter_1' => null,
                    'parameter_2' => [
                        'empty' => null
                    ],
                ]
            ),
            'parameter_1'
        );
        $this->assertEquals(
            $provider->getMissingParameter(
                [
                    'parameter_1' => true,
                    'parameter_2' => [
                        'empty' => null
                    ],
                ]
            ),
            'parameter_2.empty'
        );
        $this->assertEquals(
            $provider->getMissingParameter(
                [
                    'parameter_1' => [
                        'non_empty' => 1
                    ],
                    'parameter_2' => [
                        'empty' => null
                    ],
                ]
            ),
            'parameter_2.empty'
        );
        $this->assertEquals(
            $provider->getMissingParameter(
                [
                    'parameter_1' => [
                        'parameter_2' => [
                            'non_empty' => 1
                        ],
                        'parameter_3' => [
                            'empty' => null
                        ],
                    ],

                ]
            ),
            'parameter_1.parameter_3.empty'
        );
    }
}
