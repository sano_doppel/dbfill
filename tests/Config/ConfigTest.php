<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace DBFill\Tests;

use DBFill\Config\Config;
use PHPUnit\Framework\TestCase;

/**
 * Class ConfigTest
 */
final class ConfigTest extends TestCase
{
    const XML_FILE_PATH = __DIR__.'/config/file.xml';

    const XML_FILE2_PATH = __DIR__.'/config/file2.xml';

    /**
     * @var Config;
     */
    protected $config;

    protected function setUp()
    {
        $this->config = new Config([
            'test' => [
                'nested' => [
                    'parameter' => 'value',
                    'no_parameter' => false
                ]
            ]
        ]);
    }

    /**
     * @covers \DBFill\Config\Config::validate()
     */
    public function validate()
    {
        $this->assertEquals($this->config->validate(), false);
        $this->config->set('nested.no_parameter', true);
        $this->assertEquals($this->config->validate(), true);
    }

    /**
     * @covers \DBFill\Config\Config::get()
     */
    public function testGet()
    {
        $this->assertEquals($this->config->get('test.nested.parameter'), 'value');
    }

    /**
     * @depends testGet
     *
     * @covers \DBFill\Config\Config::set()
     */
    public function testSet()
    {
        $this->config->set('test.nested.parameter', false);
        $this->assertEquals($this->config->get('test.nested.parameter'), false);

        $this->config->set('test.nested.parameter', 'another_value');
        $this->assertEquals($this->config->get('test.nested.parameter'), 'another_value');
    }
}
