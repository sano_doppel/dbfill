<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace DBFill\Tests;

use DBFill\Config\Config;
use DBFill\Config\ConfigInterface;
use DBFill\Filler\Filler;
use DBFill\Filler\State\FillerGenerateDataState;
use DBFill\Filler\State\FillerInitState;
use DBFill\Filler\State\FillerProcessState;
use PHPUnit\Framework\TestCase;

/**
 * Class ParserTest
 */
final class FillerTest extends TestCase
{

    /**
     * @var Filler;
     */
    protected $filler;

    protected function setUp()
    {
        $this->filler = Filler::getInstance();
    }

    /**
     * @covers \DBFill\Filler\Filler::setState()
     */
    public function testSetState()
    {
        $this->filler->setState(Filler::STATE_INIT);
        $this->assertInstanceOf(FillerInitState::class, $this->filler->getState());
        $this->filler->setState(Filler::STATE_GENERATE_DATA);
        $this->assertInstanceOf(FillerGenerateDataState::class, $this->filler->getState());
        $this->filler->setState(Filler::STATE_PROCESS);
        $this->assertInstanceOf(FillerProcessState::class, $this->filler->getState());
    }

    /**
     * @covers \DBFill\Filler\Filler::init()
     */
    public function testInit()
    {
        $this->filler->init(new Config([]));
        $this->assertInstanceOf(ConfigInterface::class, $this->filler->getConfig());
        $this->assertInstanceOf(FillerInitState::class, $this->filler->getState());
    }
}
