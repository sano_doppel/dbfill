<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace DBFill\Tests;

use DBFill\Filler\Strategy\FillerStrategyInterface;
use DBFill\Filler\Strategy\SimpleStrategy;
use PHPUnit\Framework\TestCase;

/**
 * Class ParserTest
 */
final class SimpleStrategyTest extends TestCase
{

    /**
     * @var FillerStrategyInterface;
     */
    protected $strategy;

    protected function setUp()
    {
        $this->strategy = new SimpleStrategy();
    }

    /**
     * @covers \DBFill\Filler\Strategy\SimpleStrategy::parse()
     *
     * @return FillerStrategyInterface;
     */
    public function testParse()
    {
        $this->assertFalse($this->strategy->parse('no_text.txt'));
        $this->assertTrue($this->strategy->parse(__DIR__.'/text.txt'));

        return $this->strategy;
    }

    /**
     * @depends  testParse
     * @covers \DBFill\Filler\Strategy\SimpleStrategy::generate()
     * @covers \DBFill\Filler\Strategy\SimpleStrategy::getDataSize()
     *
     * @param $strategy
     *
     * @return FillerStrategyInterface;
     *
     */
    public function testGenerate(FillerStrategyInterface $strategy)
    {
        $strategy->generate();
        $this->assertEquals($strategy->getDataSize(), ['short' => 3, 'long' => 3]);

        return $strategy;
    }

    /**
     * @depends  testGenerate
     * @covers \DBFill\Filler\Strategy\SimpleStrategy::addCell()
     * @covers \DBFill\Filler\Strategy\SimpleStrategy::getRow()
     *
     * @param $strategy
     */
    public function testGetRow(FillerStrategyInterface $strategy)
    {
        $strategy->addCell('integer_cell', 'int');
        $strategy->addCell('boolean_cell', 'bool');
        $strategy->addCell('string_cell', 'string');
        $strategy->addCell('text_cell', 'text');
        $strategy->addCell('date_cell', 'date');
        $strategy->addCell('range_cell', 'range', [0, 1]);

        $row = $strategy->getRow();
        $this->assertTrue(is_int($row['integer_cell']));
        $this->assertTrue(is_string($row['string_cell']));
        $this->assertTrue(is_string($row['text_cell']));
        $this->assertTrue($row['boolean_cell'] == 0 || $row['boolean_cell'] == 1);
        $this->assertTrue($row['range_cell'] >= 0 && $row['range_cell'] <= 1);
        $this->assertTrue($row['date_cell'] < time() && $row['date_cell'] > 0);
    }
}
