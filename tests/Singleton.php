<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace DBFill\Tests;

class Singleton
{
    use \DBFill\SingletonTrait;

    private $count = 0;

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function incrementCount()
    {
        return ++$this->count;
    }

}
