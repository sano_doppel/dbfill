<?php

use DBFill\Filler\Filler;
use DBFill\Config\ConfigProvider;

require_once 'autoload.php';

$customConfig = false;
$options = getopt('c::', ['config::']);
array_walk($options, function ($item) use (&$customConfig) {
    $customConfig = $item;
});

$configProvider = new ConfigProvider();
$configProvider->createConfig($customConfig);

$configProvider->process();

$filler = Filler::getInstance();
$filler->init($configProvider->getConfig());
$filler->handle();
