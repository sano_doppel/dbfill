<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill;

/**
 * Trait SingletonTrait
 * @package DBFill
 */
trait SingletonTrait
{
    /**
     * @var self instance
     */
    static private $instance;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance() : self
    {
        return self::$instance === null ? self::$instance = new self() : self::$instance;
    }

}

