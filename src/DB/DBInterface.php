<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\DB;

/**
 * Interface ConnectionInterface
 * @package DBFill\DB
 */
interface DBInterface
{
    /**
     * Connect to database
     *
     * @param array $parameters
     *
     * @return mixed
     */
    public function connect(array $parameters);

    /**
     * Check if table is exist
     *
     * @param string $tableName
     *
     * @return bool
     */
    public function checkTable(string $tableName) : bool;

    /**
     * Insert to db
     *
     * @param string $table
     * @param array  $data
     *
     * @return mixed
     */
    public function insert(string $table, array $data) : bool;

    /**
     * Pre insert function
     */
    public function preInsert();

    /**
     * Post insert function
     */
    public function postInsert();

    /**
     * Close connection
     */
    public function close();
}
