<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\DB;

use DBFill\Messenger\Messenger;

/**
 *
 * Class DBFactory
 * @package DBFill\DB
 */
abstract class DBFactory
{
    /**
     * @param string $driver
     * @return DBInterface
     * @throws \Exception
     */
    public static function initial(string $driver) : DBInterface
    {
        switch ($driver) {
            case 'pgsql':
                return new PgSQL();
            case 'mysql':
                return new MySQL();
            default:
                throw new \Exception(Messenger::translate('exception.wrong_driver'));
        }
    }
}