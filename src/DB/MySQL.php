<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\DB;

/**
 * For MySQL
 * Class MySQL
 * @package DBFill\DB
 */
class MySQL implements DBInterface
{
    /**
     * @var \mysqli
     */
    private $connection;

    /**
     * Connect to database
     *
     * @param array $parameters
     * @return mixed
     */
    public function connect(array $parameters)
    {
        $this->connection = new \mysqli(
            $parameters['host'],
            $parameters['user'],
            $parameters['password'],
            $parameters['dbname'],
            $parameters['port']
        );
        $this->connection->set_charset("utf8");

        return $this->connection;
    }

    /**
     * Check if table is exist
     *
     * @param string $tableName
     *
     * @return bool
     */
    public function checkTable(string $tableName) : bool
    {
        $stmt = $this->connection->prepare(
            "SELECT EXISTS (SELECT 1 FROM information_schema.TABLES WHERE table_name = ?) as has_table"
        );

        $hasTable = false;

        $stmt->bind_param('s', $tableName);
        $stmt->execute();

        $stmt->bind_result($hasTable);
        $stmt->fetch();
        $stmt->close();

        return boolval($hasTable);
    }

    /**
     * Insert to db
     *
     * @param string $table
     * @param array  $data
     *
     * @return mixed
     */
    public function insert(string $table, array $data) : bool
    {
        $keys = array_map(function ($value) {
            return sprintf("`%s`", $value);
        }, array_keys($data));

        $values = array_map(function ($value) {
            $value = $this->connection->real_escape_string($value);
            return is_string($value) ? sprintf("'%s'", $value) : $value;
        }, array_values($data));

        $sql = sprintf("INSERT INTO `%s` (%s) VALUES (%s)", $table, implode(',', $keys), implode(',', $values));

        return boolval($this->connection->query($sql));
    }

    /**
     * Pre insert function
     */
    public function preInsert()
    {
        $this->connection->begin_transaction();
    }

    /**
     * Post insert function
     */
    public function postInsert()
    {
        $this->connection->commit();
    }


    /**
     * close connection
     */
    public function close()
    {
        $this->connection->close();
    }
}
