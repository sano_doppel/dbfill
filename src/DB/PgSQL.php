<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\DB;

/**
 * For PostgreSQL
 * Class PgSQL
 * @package DBFill\DB
 */
class PgSQL implements DBInterface
{
    /**
     * @var
     */
    private $connection;

    /**
     * Connect to database
     *
     * @param array $parameters
     * @return mixed
     */
    public function connect(array $parameters)
    {
        $this->connection = pg_connect(
            sprintf(
                "host=%s port=%s dbname=%s user=%s password=%s",
                $parameters['host'],
                $parameters['port'],
                $parameters['dbname'],
                $parameters['user'],
                $parameters['password']
            )
        );

        return $this->connection;
    }

    /**
     * Check if table is exist
     *
     * @param string $tableName
     *
     * @return bool
     */
    public function checkTable(string $tableName) : bool
    {
        $queryId = sprintf("exist_%s", $tableName);
        $tableName = explode('.', $tableName);

        pg_prepare(
            $this->connection,
            $queryId,
            "SELECT EXISTS (
              SELECT 1
              FROM   information_schema.tables 
              WHERE  table_schema = $1
              AND table_name = $2
            )::INT as has_table"
        );

        $result = pg_execute($this->connection, $queryId, [
            sizeof($tableName) > 1 ? $tableName[0] : 'public',
            sizeof($tableName) > 1 ? $tableName[1] : $tableName[0]
        ]);

        return pg_fetch_assoc($result)['has_table'];
    }

    /**
     * Insert to db
     *
     * @param string $table
     * @param array  $data
     *
     * @return mixed
     */
    public function insert(string $table, array $data) : bool
    {
        return boolval(
            pg_insert(
                $this->connection,
                $table,
                $data
            )
        );
    }

    /**
     * Pre insert function
     */
    public function preInsert()
    {
        pg_query($this->connection, 'START TRANSACTION');
    }

    /**
     * Post insert function
     */
    public function postInsert()
    {
        pg_query($this->connection, 'COMMIT');
    }

    /**
     * close connection
     */
    public function close()
    {
        pg_close($this->connection);
    }
}
