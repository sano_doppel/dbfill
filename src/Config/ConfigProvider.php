<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Config;

use DBFill\Config\Parser\Parser;

/**
 * Class ConfigProvider
 * @package DBFill\Config
 */
class ConfigProvider
{
    const PARAMETERS_PATH = __DIR__ . '/parameters.json';

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @param string $path
     *
     * @return ConfigInterface
     */
    public function createConfig(string $path) : ConfigInterface
    {
        $parameters = Parser::initial(self::PARAMETERS_PATH)->parse(Parser::OUTPUT_TYPE_ARRAY);
        $initConfig =  array_merge($parameters['non_editable'], $parameters['editable']);
        $customConfig = $path ? Parser::initial($path)->parse(Parser::OUTPUT_TYPE_ARRAY) : false;
        $this->config = new Config(
            $customConfig ? $this->mergeConfigs($initConfig, $customConfig, $parameters['non_editable']) : $initConfig
        );

        return $this->config;
    }

    /**
     * @return ConfigInterface
     */
    public function getConfig() : ?ConfigInterface
    {
        return $this->config;
    }

    /**
     * Checks config for missing parameters and set them from CLI
     */
    public function process()
    {
        while (!$this->config->validate()) {
            $missing = $this->getMissingParameter($this->config->getParameters());
            if (!$missing) {
                break;
            }

            $line = readline(sprintf('%s: ', $missing));
            $this->config->set($missing, $line);
        }
    }


    /**
     * Get missing parameter from config parameters
     *
     * @param array $parameters
     * @param array $fullName
     *
     * @return string
     */
    public function getMissingParameter(array $parameters, array $fullName = []) :? string
    {

        $missing = null;
        foreach ($parameters as $key => $childrenParameter) {
            if (is_array($childrenParameter)) {
                $tempName = $fullName;
                $fullName[] = $key;
                $missing = $this->getMissingParameter($childrenParameter, $fullName);
                if ($missing) {
                    return $missing;
                }
                $fullName = $tempName;
            } elseif (is_null($childrenParameter)) {
                $fullName[] = $key;
                return implode('.', $fullName);
            }
        }

        return $missing;
    }

    /**
     * @param array $initConfig
     * @param array $customConfig
     * @param array $nonEditable
     * @return array
     */
    private function mergeConfigs(array $initConfig, array $customConfig, array $nonEditable) : array
    {
        foreach ($initConfig as $key => $parameter) {
            if (isset($nonEditable[$key]) and isset($customConfig[$key])) {
                $initConfig[$key] = $customConfig[$key];
            }
        }

        return $this->mergeEditableData($initConfig, $customConfig);
    }

    /**
     * Merge init and custom configs
     *
     * @param array $initConfig
     * @param array $customConfig
     *
     * @return array
     */
    private function mergeEditableData(array $initConfig, array $customConfig) : array
    {

        foreach ($initConfig as $key => $parameter) {
            if (is_array($parameter) and isset($customConfig[$key])) {
                $initConfig[$key] = $this->mergeEditableData($parameter, $customConfig[$key]);
            } elseif (isset($customConfig[$key])) {
                $initConfig[$key] = $customConfig[$key];
            }
        }

        return $initConfig;
    }
}

