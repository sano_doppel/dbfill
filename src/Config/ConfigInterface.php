<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Config;

/**
 * Interface ConfigInterface
 * @package DBFill\Config
 */
interface ConfigInterface
{
    /**
     * Get parameters
     *
     * @return array
     */
    public function getParameters();

    /**
     * Add parameter to Config
     * @param string $key
     * @param mixed  $parameter
     *
     * @return $this
     */
    public function set(string $key, $parameter);

    /**
     * Get parameter
     * @param string $key
     * @return string
     */
    public function get(string $key) :? string;

    /**
     * Config parameters validation
     * @return bool
     */
    public function validate();
}
