<?php

namespace DBFill\Config\Parser;

/**
 * Class JSONParser
 * @package DBFill\Config\Parser
 */
class JSONParser extends Parser
{
    /**
     * @var string
     */
    private $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @param int $outputType
     * @return array|object
     */
    public function parse(int $outputType = self::OUTPUT_TYPE_OBJECT)
    {
        return json_decode(file_get_contents($this->path), $outputType);
    }
}

