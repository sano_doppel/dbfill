<?php

namespace DBFill\Config\Parser;

/**
 * Class XMLParser
 * @package DBFill\Config\Parser
 */
class XMLParser extends Parser
{
    /**
     * @var string
     */
    private $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @param int $outputType
     *
     * @return array|object
     */
    public function parse(int $outputType = self::OUTPUT_TYPE_OBJECT)
    {
        switch ($outputType) {
            case self::OUTPUT_TYPE_OBJECT:
                return simplexml_load_file($this->path);
            case self::OUTPUT_TYPE_ARRAY:
                return $this->convertXMLToArray(simplexml_load_file($this->path));
        }
    }


    /**
     * @param \SimpleXMLElement $object
     * @param array  $result
     *
     * @return array
     */
    private function convertXMLToArray(\SimpleXMLElement $object, array $result = []) : array
    {
        $array = (array)$object;
        if (empty($array)) {
            return [];
        }
        
        foreach ($array as $key => $item) {
            $result[$key] = is_object($item) ? $this->convertXMLToArray($item) : $item;
        }

        return $result;
    }
}

