<?php

namespace DBFill\Config\Parser;

use DBFill\Messenger\Messenger;

/**
 * Class Parser factory
 * @package DBFill\Config\Parser
 */
abstract class Parser implements ParserInterface
{
    const OUTPUT_TYPE_OBJECT = 0;

    const OUTPUT_TYPE_ARRAY = 1;

    /**
     * @param  string $path
     * @param bool $showError
     * @return ParserInterface
     *
     * @throws \Exception
     */
    public static function initial(string $path, bool $showError = true) : ParserInterface
    {
        if (!is_readable($path)) {
            if (!$showError) {
                return false;
            }
            throw new \Exception(sprintf(Messenger::translate('exception.parser_wrong_path'), $path));
        }

        preg_match('/\.\w+$/', $path, $matches);
        switch (ltrim(isset($matches[0]) ? $matches[0] : '', '.')) {
            case 'xml':
                return new XMLParser($path);
            case 'json':
                return new JSONParser($path);
            default:
                if (!$showError) {
                    return false;
                }
                throw new \Exception(Messenger::translate('exception.config_wrong_extension'));
        }
    }

    abstract public function parse(int $outputType);
}
