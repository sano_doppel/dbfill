<?php

namespace DBFill\Config\Parser;

/**
 * Interface ParserInterface
 * @package DBFill\Config\Parser
 */
interface ParserInterface
{
    /**
     * @param int $outputType
     * @return array | object
     */
    public function parse(int $outputType);
}
