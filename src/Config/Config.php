<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Config;

/**
 * Class Config
 * @package DBFill\Config
 */
class Config implements ConfigInterface
{
    const KEY_SEPARATOR = '.';

    /**
     * @var array
     */
    protected $parameters;

    /**
     * Config constructor.
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * Add parameter to Config
     * @param string $key
     * @param mixed  $parameter
     *
     * @return $this
     */
    public function set(string $key, $parameter) : self
    {
        $this->parameters = $this->setNewArgument(explode(self::KEY_SEPARATOR, $key), $this->parameters, $parameter);

        return $this;
    }

    /**
     * Get parameter
     * @param string $key
     * @return mixed
     */
    public function get(string $key) :? string
    {
        return $this->findParameter(explode(self::KEY_SEPARATOR, $key), $this->parameters);
    }

    /**
     * Get parameters
     *
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Config parameters validation
     * @return bool
     */
    public function validate() : bool
    {
        $isValid = true;
        array_walk_recursive($this->parameters, function ($item) use (&$isValid) {
            if (empty($item)) {
                $isValid = false;
            }
        });

        return $isValid;
    }

    /**
     * Recursion finds parameter in parameters array and get value
     * @param array $keyDepth
     * @param array  $parameters
     *
     * @return mixed
     */
    private function findParameter(array $keyDepth, array $parameters)
    {
        $currentKey = array_shift($keyDepth);
        if (!$currentKey) {
            return null;
        }

        foreach ($parameters as $parameterKey => $childrenParameter) {
            if ($parameterKey === $currentKey and is_array($childrenParameter)) {
                return $this->findParameter($keyDepth, $childrenParameter);
            } elseif ($parameterKey === $currentKey) {
                return $childrenParameter;
            }
        }

        return null;
    }

    /**
     * Finds parameter in parameters array and set a new value
     * @param array  $keyDepth
     * @param array  $parameters
     * @param string $newArgument
     *
     * @return array
     */
    private function setNewArgument(array $keyDepth, array $parameters, string $newArgument) : array
    {
        $currentKey = array_shift($keyDepth);
        if (!$currentKey) {
            return $parameters;
        }

        foreach ($parameters as $parameterKey => $childrenParameter) {
            if ($parameterKey === $currentKey and is_array($childrenParameter)) {
                $parameters[$parameterKey] = $this->setNewArgument($keyDepth, $childrenParameter, $newArgument);
                break;
            } elseif ($parameterKey === $currentKey) {
                $parameters[$parameterKey] = $newArgument;
                break;
            }
        }

        return $parameters;
    }
}
