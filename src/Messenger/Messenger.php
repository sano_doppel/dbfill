<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Messenger;

use DBFill\Messenger\TranslationFinder\NestedStrategy;
use DBFill\Messenger\TranslationFinder\TranslationFinderInterface;

/**
 * Class Messenger
 * @package DBFill\Messenger
 */
class Messenger
{
    /**
     * get translated message from MessageRepository
     *
     * @param string                     $message
     * @param string|null                $locale
     * @param TranslationFinderInterface $strategy
     * @return string
     */
    public static function translate(string $message, $locale = null, TranslationFinderInterface $strategy = null) : string
    {
        $messageRepository = MessageRepository::getInstance();
        if (!$messageRepository->getStrategy() and !$strategy) {
            $strategy = new NestedStrategy();
        }

        if ($strategy) {
            $messageRepository->setStrategy($strategy);
        }

        return (MessageRepository::getInstance())->getMessage($message, $locale);
    }

    /**
     * @param array $parameters
     */
    public static function setTranslationParameters(array $parameters)
    {
        $instanse = MessageRepository::getInstance();
        foreach ($parameters as $key => $parameter) {
            $setter = sprintf('set%s', ucfirst($key));
            if (method_exists($instanse, $setter)) {
                $instanse->$setter($parameter);
            }
        }
    }
}
