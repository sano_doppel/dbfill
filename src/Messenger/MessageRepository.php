<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Messenger;

use DBFill\Config\Parser\Parser;
use DBFill\Messenger\TranslationFinder\TranslationFinderInterface;
use DBFill\SingletonTrait;

/**
 * Class MessageRepository
 * @package DBFill\Messenger
 */
class MessageRepository
{
    use SingletonTrait;

    /**
     * @var string
     */
    private $translationsExtension = 'json';

    /**
     * @var string
     */
    private $translationsPrefix = 'messages';

    /**
     * @var string
     */
    private $translationsDir = __DIR__;

    /**
     * @var self default locale
     */
    private $defaultLocale = 'en';

    /**
     * @var $translations array
     */
    private $translations = [];

    /**
     * @var TranslationFinderInterface
     */
    private $strategy;

    /**
     * get translated message
     * @param string      $message
     * @param string|null $locale
     *
     * @return string
     */
    public function getMessage(string $message, $locale = null) : ?string
    {
        if (!isset($this->translations[$locale])) {
            $this->translations[$locale] = $this->getTranslationsForLocale($locale);
        }

        return $this->strategy->findInObject($message, $this->translations[$locale]);
    }

    /**
     * @param TranslationFinderInterface $strategy
     *
     * @return $this
     */
    public function setStrategy(TranslationFinderInterface $strategy) : self
    {
        $this->strategy = $strategy;

        return $this;
    }

    /**
     * @return TranslationFinderInterface
     */
    public function getStrategy() : ?TranslationFinderInterface
    {
        return $this->strategy;
    }

    /**
     * @param string $translationsExtension
     *
     * @return $this
     */
    public function setTranslationsExtension(string $translationsExtension) : self
    {
        $this->translationsExtension = $translationsExtension;
        $this->translations = [];

        return $this;
    }

    /**
     * @param string $translationsPrefix
     *
     * @return $this
     */
    public function setTranslationsPrefix(string $translationsPrefix) : self
    {
        $this->translationsPrefix = $translationsPrefix;
        $this->translations = [];

        return $this;
    }

    /**
     * @param string $translationsDir
     *
     * @return $this
     */
    public function setTranslationsDir(string $translationsDir) : self
    {
        $this->translationsDir = $translationsDir;
        $this->translations = [];

        return $this;
    }

    /**
     * @param string $defaultLocale
     *
     * @return $this
     */
    public function setDefaultLocale(string $defaultLocale) : self
    {
        $this->defaultLocale = $defaultLocale;

        return $this;
    }

    /**
     * get translations for locale
     *
     * @return object
     *
     * @throws \Exception
     */
    private function getTranslationsForLocale()
    {
        $parser = Parser::initial(
            sprintf(
                '%s/%s.%s.%s',
                $this->translationsDir,
                $this->translationsPrefix,
                $this->defaultLocale,
                $this->translationsExtension
            ),
            false
        );
        if (!$parser) {
            throw new \Exception('Parser Error');
        }

        return $parser->parse(Parser::OUTPUT_TYPE_OBJECT);
    }
}
