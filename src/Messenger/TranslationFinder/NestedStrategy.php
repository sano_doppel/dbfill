<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Messenger\TranslationFinder;

/**
 * Class NestedStrategy
 * @package DBFill\Messenger\TranslationFinder
 */
class NestedStrategy implements TranslationFinderInterface
{
    /**
     * @var string
     */
    private $separator;

    /**
     * NestedStrategy constructor.
     * @param string $separator
     */
    public function __construct(string $separator = '.')
    {
        $this->separator = $separator;
    }

    /**
     * Find Message in object
     * @param string $message
     * @param \stdClass $object
     *
     * @return string
     */
    public function findInObject(string $message, \stdClass $object) : string
    {
        $explodedMessage = explode($this->separator, $message);
        $currentLevel = array_shift($explodedMessage);

        if (!$currentLevel or !property_exists($object, $currentLevel)) {
            return $message;
        }

        return is_object($object->{$currentLevel}) ?
            $this->findInObject(implode($this->separator, $explodedMessage), $object->{$currentLevel}) :
            $object->{$currentLevel};
    }

    /**
     * Find Message in array
     * @param string $message
     * @param array $array
     *
     * @return string
     */
    public function findInArray(string $message, array $array) : string
    {
        $explodedMessage = explode($this->separator, $message);
        $currentLevel = array_shift($explodedMessage);

        if (!isset($array[$message])) {
            return $message;
        }

        return is_array($array[$currentLevel]) ?
            $this->findInArray(implode($this->separator, $explodedMessage), $array[$currentLevel]) :
            $array[$currentLevel];
    }
}
