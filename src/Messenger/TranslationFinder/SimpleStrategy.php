<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Messenger\TranslationFinder;

/**
 * Class NestedStrategy
 * @package DBFill\Messenger\TranslationFinder
 */
class SimpleStrategy implements TranslationFinderInterface
{
    /**
     * Find Message in object
     * @param string $message
     * @param \stdClass $object
     *
     * @return string
     */
    public function findInObject(string $message, \stdClass $object) : string
    {
        if (!property_exists($object, $message)) {
            return $message;
        }

        return $object->{$message};
    }

    /**
     * Find Message in array
     * @param string $message
     * @param array $array
     *
     * @return string
     */
    public function findInArray(string $message, array $array) : string
    {
        if (!isset($array[$message])) {
            return $message;
        }

        return $array[$message];
    }
}
