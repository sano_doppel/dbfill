<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Filler\State;

use DBFill\Filler\Filler;

/**
 * Class FillerInitState
 * @package DBFill\Filler\State
 */
class FillerInitState extends AbstractFillerState
{
    /**
     * Init db fill
     */
    public function handle()
    {
        $parameters = $this->filler->getConfig()->getParameters();
        if (!$this->filler->getConfig()->validate()) {
            $this->filler->message($this->filler->translate('filler.error.wrong_config'));
            $this->filler->stop();

        } elseif (!isset($parameters['tables']) or !$parameters['tables']) {
            $this->filler->message($this->filler->translate('filler.error.no_tables'));
            $this->filler->stop();
        } else {
            $this->filler->connect();
            $this->filler->setState(Filler::STATE_GENERATE_DATA);
        }
    }
}
