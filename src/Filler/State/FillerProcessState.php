<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Filler\State;

/**
 * Class FillerProcessState
 * @package DBFill\Filler\State
 */
class FillerProcessState extends AbstractFillerState
{
    const CHUNK_SIZE = 20;

    /**
     * Process db fill
     */
    public function handle()
    {
        if (!$this->filler->getConnection()) {
            return false;
        }

        $parameters = $this->filler->getConfig()->getParameters();
        foreach ($parameters['tables'] as $tableName => $table) {
            $this->filler->getFillerStrategy()->clearCells();
            if (!$this->checkForTable($tableName) or !$this->checkForCells($tableName, $table)) {
                continue;
            }

            $this->createCells($table['cells']);
            $this->addToTable($tableName, isset($table['limit']) ? (int) $table['limit'] : 0);
        }
        $this->filler->getConnection()->close();
    }

    /**
     * Check if table is exists
     *
     * @param string $tableName
     *
     * @return bool
     */
    private function checkForTable($tableName)
    {
        if (!$this->filler->getConnection()->checkTable($tableName)) {
            $this->filler->message(
                sprintf(
                    $this->filler->translate('filler.error.no_table'),
                    $tableName
                )
            );

            return false;
        }

        return true;
    }

    /**
     * Checks if table cells are configured
     *
     * @param string $tableName
     * @param array  $table
     *
     * @return bool
     */
    private function checkForCells($tableName, $table)
    {
        if (!isset($table['cells']) or !$table['cells']) {
            $this->filler->message(
                sprintf(
                    $this->filler->translate('filler.error.no_cells_in_table'),
                    $tableName
                )
            );

            return false;
        }
        foreach ($table['cells'] as $cellName => $cell) {
            if (!$cell['type']) {
                $this->filler->message(
                    sprintf(
                        $this->filler->translate('filler.error.ceil_type_undefined'),
                        $tableName,
                        $cellName
                    )
                );

                return false;
            }
        }

        return true;
    }

    /**
     * Create cells
     *
     * @param array $cells
     */
    private function createCells($cells)
    {
        foreach ($cells as $cellName => $cell) {
            $this->filler->getFillerStrategy()->addCell(
                $cellName,
                $cell['type'],
                isset($cell['data']) ? $cell['data'] : null
            );
        }
    }

    /**
     * Add rows to table
     *
     * @param string $tableName
     * @param int    $limit
     */
    private function addToTable($tableName, $limit)
    {
        $this->filler->getConnection()->preInsert();
        $addedToTable = 0;
        $chunkCount = 0;
        for ($count = 0; $count < $limit; $count++) {
            $chunkCount++;
            $addedToTable += $this->filler->getConnection()
                ->insert($tableName, $this->filler->getFillerStrategy()->getRow());

            if ($chunkCount == self::CHUNK_SIZE) {
                $this->filler->message(
                    sprintf(
                        $this->filler->translate('filler.result.process'),
                        $tableName,
                        (($count + 1) * 100 / $limit)
                    )
                );
                $chunkCount = 0;
            }
        }
        $this->filler->message(
            sprintf(
                $this->filler->translate('filler.result.total'),
                $tableName,
                $addedToTable
            )
        );
        $this->filler->getConnection()->postInsert();
    }
}
