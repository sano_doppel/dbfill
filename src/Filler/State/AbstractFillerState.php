<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Filler\State;

use DBFill\Filler\Filler;

abstract class AbstractFillerState implements FillerStateInterface
{
    /**
     * @var Filler
     */
    protected $filler;

    /**
     * AbstractFillerState constructor.
     * @param Filler $filler
     */
    public function __construct(Filler &$filler)
    {
        $this->filler = &$filler;
    }


    abstract public function handle();

}
