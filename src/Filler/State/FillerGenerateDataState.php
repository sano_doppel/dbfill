<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Filler\State;

use DBFill\Filler\Filler;

/**
 * Class FillerReadyState
 * @package DBFill\Filler\State
 */
class FillerGenerateDataState extends AbstractFillerState
{

    /**
     * Process db fill
     */
    public function handle()
    {
        $strategyClass = ($this->filler->getConfig()->get('strategy'));
        $this->filler->setFillerStrategy(new $strategyClass);
        if (!$this->filler->getFillerStrategy()->parse($this->filler->getConfig()->get('filler.path'))) {
            $this->filler->message($this->filler->translate('filler.error.wrong_parsed_file_path'));
            $this->filler->stop();
        } else {
            $this->filler->getFillerStrategy()->generate();
            $this->filler->setState(Filler::STATE_PROCESS);
        }
    }
}
