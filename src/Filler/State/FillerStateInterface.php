<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Filler\State;

/**
 * Interface StateInterface
 * @package DBFill\State
 */
interface FillerStateInterface
{
    public function handle();
}
