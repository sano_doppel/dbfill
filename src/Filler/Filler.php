<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Filler;

use DBFill\Config\ConfigInterface;
use DBFill\DB\DBFactory;
use DBFill\DB\DBInterface;
use DBFill\Filler\State\FillerGenerateDataState;
use DBFill\Filler\State\FillerInitState;
use DBFill\Filler\State\FillerProcessState;
use DBFill\Filler\State\FillerStateInterface;
use DBFill\Filler\Strategy\FillerStrategyInterface;
use DBFill\Messenger\Messenger;
use DBFill\SingletonTrait;

/**
 * Class Filler
 * @package DBFill
 */
class Filler
{
    use SingletonTrait;

    const STATE_INIT = 1;
    const STATE_GENERATE_DATA = 2;
    const STATE_PROCESS = 3;

    /**
     * @var DBInterface
     */
    private $connection;

    /**
     * @var FillerStrategyInterface
     */
    private $fillerStrategy;

    /**
     * @var string
     */
    private $message;

    /**
     * @var FillerStateInterface
     */
    private $state;

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var int
     */
    private $hasStopped = false;

    /**
     * @var int
     */
    private $currentStateCode;

    public function setState(int $state)
    {
        switch ($state) {
            case self::STATE_INIT:
                $this->state = new FillerInitState($this);
                break;
            case self::STATE_GENERATE_DATA:
                $this->state = new FillerGenerateDataState($this);
                break;
            case self::STATE_PROCESS:
                $this->state = new FillerProcessState($this);
                break;
        }

        $this->currentStateCode = $state;
    }

    /**
     * @return FillerStateInterface
     */
    public function getState() : FillerStateInterface
    {
        return $this->state;
    }


    /**
     * @return string
     */
    public function getMessage() : string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /**
     * @param FillerStrategyInterface $fillerStrategy
     *
     * @return $this
     */
    public function setFillerStrategy(FillerStrategyInterface $fillerStrategy)
    {
        $this->fillerStrategy = $fillerStrategy;

        return $this;
    }

    /**
     * @return FillerStrategyInterface
     */
    public function getFillerStrategy()
    {
        return $this->fillerStrategy;
    }

    /**
     * connect to database
     */
    public function connect()
    {
        $this->connection = DBFactory::initial(
            $this->config->get('database.driver')
        );
        $connectId = $this->connection->connect([
            'host' => $this->config->get('database.host'),
            'port' => $this->config->get('database.port'),
            'user' => $this->config->get('database.user'),
            'password' => $this->config->get('database.password'),
            'dbname' => $this->config->get('database.dbname'),
        ]);

        if (!$connectId) {
            $this->connection = null;
        }
    }

    /**
     * @return DBInterface
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param ConfigInterface $config
     */
    public function init(ConfigInterface $config)
    {
        $this->config = $config;
        $this->setState(self::STATE_INIT);
    }

    /**
     * @return ConfigInterface
     */
    public function getConfig() : ConfigInterface
    {
        return $this->config;
    }

    public function handle()
    {
        foreach ([self::STATE_INIT, self::STATE_GENERATE_DATA, self::STATE_PROCESS] as $currentState) {
            if ($this->currentStateCode !== $currentState or $this->hasStopped) {
                break;
            }
            $this->state->handle();
        }
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public function translate(string $text) : string
    {
        return Messenger::translate($text);
    }

    /**
     * @param string $text
     */
    public function message(string $text)
    {
        echo $text.PHP_EOL;
    }

    public function stop()
    {
        $this->hasStopped = true;
    }
}
