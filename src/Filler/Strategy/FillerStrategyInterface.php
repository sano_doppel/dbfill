<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Filler\Strategy;

/**
 * Interface FillerStrategyInterface
 * @package DBFill\Filler\Strategy
 */
interface FillerStrategyInterface
{
    /**
     * Parse the sourse
     *
     * @param string $path
     *
     * @return bool
     */
    public function parse(string $path) : bool;

    /**
     * Generate random data
     */
    public function generate();

    /**
     * Add data cell
     *
     * @param string $name
     * @param string $type
     * @param null $data
     */
    public function addCell(string $name, string $type, $data = null);

    /**
     * Get new row
     *
     * @return array
     */
    public function getRow() : array;

    /**
     * @return mixed
     */
    public function getDataSize();

    /**
     * Clear cells
     */
    public function clearCells();

}
