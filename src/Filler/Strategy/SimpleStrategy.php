<?php
/**
 * @author <sanodoppel@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DBFill\Filler\Strategy;

use DBFill\Messenger\Messenger;

class SimpleStrategy implements FillerStrategyInterface
{

    const SHORT_DELIMITER = "/ /";
    const SHORT_DATA_LIMIT = 4096;
    const SHORT_DATA_MIN_LETTERS = 3;
    const INT_MAX = 2048;
    const TYPE = ['bool' => 1, 'int' => 2, 'string' => 3, 'text' => 4, 'date' => 5, 'datetime' => 6, 'range' => 7];

    /**
     * @var array
     */
    private $cells = [];

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $sourceData;

    /**
     * @var array
     */
    private $data = [
        'short' => [
            'data' => [],
            'size' => 0,
        ],
        'long' => [
            'data' => [],
            'size' => 0,
        ]
    ];

    /**
     * Generate random data
     */
    public function generate()
    {
        $this->data['long']['data'] = file($this->path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $this->data['long']['size'] = sizeof($this->data['long']['data']);

        preg_match_all(
            sprintf("/\b[\p{L}0-9]{%s,}\b/u", self::SHORT_DATA_MIN_LETTERS),
            substr($this->sourceData, 0, self::SHORT_DATA_LIMIT),
            $shortData,
            PREG_PATTERN_ORDER
        );
        if ($shortData) {
            $this->data['short']['data'] = $shortData[0];
            $this->data['short']['size'] = sizeof($this->data['short']['data']);
        }
    }

    /**
     * Parse file
     * @param string $path
     *
     * @return bool
     *
     */
    public function parse(string $path) : bool
    {

        if (is_file($path)) {
            $this->sourceData = file_get_contents($path);
            $this->path = $path;
            return true;
        }

        return false;
    }

    /**
     * Add data cell
     *
     * @param string $name
     * @param string $type
     * @param null $data
     *
     * @throws \Exception
     */
    public function addCell(string $name, string $type, $data = null)
    {
        if (!isset(self::TYPE[strtolower($type)])) {
            throw new \Exception(sprintf(Messenger::translate('filler.error.wrong_cell_type'), $type));
        }
        $this->cells[$name] = [
            'type' => $type,
            'data' => $data,
        ];
    }

    /**
     * Get new row
     *
     * @return array
     */
    public function getRow() : array
    {
        $data = [];
        foreach ($this->cells as $key => $cell) {
            $data[$key] = $this->getData($cell);
        }

        return $data;
    }

    public function clearCells()
    {
        $this->cells = [];
    }

    /**
     * @return array
     */
    public function getDataSize()
    {
        return ['short' => $this->data['short']['size'], 'long' => $this->data['long']['size']];
    }

    /**
     * @param array $cell
     *
     * @return mixed
     */
    private function getData(array $cell)
    {
        switch (self::TYPE[strtolower($cell['type'])]) {
            case self::TYPE['bool']:
                return rand(0, 1);
            case self::TYPE['int']:
                return rand(0, self::INT_MAX);
            case self::TYPE['string']:
                return $this->data['short']['data'][rand(0, $this->data['short']['size'] - 1)];
            case self::TYPE['text']:
                return $this->data['long']['data'][rand(0, $this->data['long']['size'] - 1)];
            case self::TYPE['date']:
                return date('Y-m-d', rand(0, time()));
            case self::TYPE['datetime']:
                return date('Y-m-d H:i:s', rand(0, time()));
            case self::TYPE['range']:
                return rand(
                    isset($cell['data'][0]) ? (int) $cell['data'][0] : 0,
                    isset($cell['data'][1]) ? (int) $cell['data'][1] : 1
                );
            default:
                return false;
        }
    }
}
