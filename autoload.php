<?php

use DBFill\Messenger\Messenger;

spl_autoload_register(function ($class) {
    $filename = __DIR__.'/' . str_replace('\\', '/', str_replace('DBFill', 'src', $class)) . '.php';
    if (!is_file($filename)) {
        throw new Exception(sprintf(Messenger::translate('exception.class_not_found'), $filename));
    }
    include_once($filename);

});